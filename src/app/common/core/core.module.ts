import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  exports: [HomeComponent],
  declarations: [HomeComponent, AlertComponent]
})
export class CoreModule { }
